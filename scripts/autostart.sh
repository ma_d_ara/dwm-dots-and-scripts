sxhkd -c ~/.dwm/sxhkdrc &
setxkbmap -layout pl &
nm-applet &
lxpolkit &
xss-lock -- ~/.config/lock.sh &
dash ~/.conky/conky-startup.sh &
nitrogen --restore &
blueman-applet &
i3-battery-popup -L 15 -n -s ~/.config/i3-battery-popup.wav -v 50 &dash ~/.config/restart.sh &
copyq &
dunst &
dash ~/.dwm/stat.sh

