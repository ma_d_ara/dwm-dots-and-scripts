#!/bin/bash

sep=" | " #separator dividing functions
battery(){ #link to battery script
./batstate.sh
}

date(){ # link to date scrip
./date.sh
}

volumeLevel(){ # link to vol script
./vol.sh
}

backlight(){ # linkt to light script
./light.sh
}

stor(){ # link to storage script
./storage.sh
}



update(){ # updating status bar using scipts above
xsetroot -name "$(stor)$sep $(backlight)$sep$(battery)$sep $(volumeLevel)$sep$(date)" 
}


# loop which update bar every minute
while :; do
update
sleep 1m
done
