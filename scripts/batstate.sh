#!/bin/bash

state(){
upower -i $(upower -e | grep 'BAT') | grep "state:"
}
cap(){
cat /sys/class/power_supply/BAT0/capacity
}

if [[ $(state) == *fully-charged* ]]
then
	echo "  $(cat /sys/class/power_supply/BAT0/capacity)%"
elif [[ $(state) == *discharging* ]]
then
	if (( $(cap) <= 20 ))
	then
		echo  -e "\033[5m\033[0m $(cat /sys/class/power_supply/BAT0/capacity)%"
	elif (( $(cap) > 20 && $(cap) < 49 ))
	then
		echo "  $(cat /sys/class/power_supply/BAT0/capacity)%"
	elif (( $(cap) >= 50 && $(cap) < 70 ))
		then 
			echo "  $(cat /sys/class/power_supply/BAT0/capacity)%"
	elif (( $(cap) >= 70 && $(cap) < 85 ))
		then
			echo "  $(cat /sys/class/power_supply/BAT0/capacity)%"
	else
			echo "  $(cat /sys/class/power_supply/BAT0/capacity)%"
	fi
elif [[ $(state) == *charging* ]]
then
	if (( $(cap) <= 20 ))
	then
		echo  -e " \033[5m\033[0m $(cat /sys/class/power_supply/BAT0/capacity)%"
	elif (( $(cap) > 20 && $(cap) < 49 ))
	then
		echo "   $(cat /sys/class/power_supply/BAT0/capacity)%"
	elif (( $(cap) >= 50 && $(cap) < 70 ))
		then 
			echo "   $(cat /sys/class/power_supply/BAT0/capacity)%"
	elif (( $(cap) >= 70 && $(cap) < 85 ))
		then
			echo "   $(cat /sys/class/power_supply/BAT0/capacity)%"
	else
			echo "   $(cat /sys/class/power_supply/BAT0/capacity)%"
	fi
else
	echo "No battery or script error"
fi
